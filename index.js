import { connect } from 'mongoose'
import bodyParser from 'body-parser'
import express from 'express'
import UserModel from './UserModel'
import cors from 'cors'

connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo', { useNewUrlParser: true })

const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', (req, res) => { // A sample api usefull to test if the server is up
  res.send('Just a test')
})

app.get('/users', (req, res) => { // This endpoint return the list of all users in the db, usually you can find it in the admin shell
  UserModel.find((err, results) => {
    res.send(results)
  })
})

app.post('/users', (req, res) => { // This endpoint add a new user in the db
  let user = new UserModel() // creatione of user like the model of the user table

  user.email = req.body.email // assignnments of user data
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  user.save((err, newUser) => { // then the call to save function, wich can return an error or the user data 
    if (err) res.send(err)
    else res.send(newUser)
  })
})

app.get('/users/id', (req, res) => { // This endpoint find and return a user by his mongoid, instead of the query object you can use the mongoid to find a specifically element
  UserModel.findone(req.body._id,(err, userData) => {
    if (err) res.send(err)
    else res.send(userData)
  })
})
  
app.delete('/users/id', (req, res) => { // This endpoint find and delete a user by his mongoid
  UserModel.findOneAndDelete(req.body._id,(err, results) => {
    if (err) res.send(err)
    else res.send(results)
  })
})

app.listen(8080, () => console.log('Example app listening on port 8080!')) // Access port to the api
