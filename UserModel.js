import { Schema, model } from 'mongoose'

const UserSchema = new Schema({ // Creation of user schema using mongoose schema
  email: { type: String, lowercase: true, trim: true, required: true, unique: true }, // during the schema creation you can specify the attributes of the single field, like this field wich is unique and required, and force the data formatting by using lowercase and trim 
  password: { type: String, required: true, select: false }, // select is default true and make field value accessible, by setting it on false the value is not accessible
  firstName: { type: String, required: true },
  lastName: { type: String, required: true }
})

export default model('User', UserSchema)
